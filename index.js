const express = require('express');
const cors = require('cors');
const fs = require('fs');
const app = express();
const PORT = 3002; // Port à écouter
const path = require('path');
const axios = require("axios");
const FormData = require("form-data");
const JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiI3N2U5ZmZlMi1jYTBiLTQyZmQtODQyZC00NTM1ODViNGNmODAiLCJlbWFpbCI6ImFsZXhhbmRyZS5uaWNhaXNlQHNvZnRlYW0uZnIiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicGluX3BvbGljeSI6eyJyZWdpb25zIjpbeyJpZCI6IkZSQTEiLCJkZXNpcmVkUmVwbGljYXRpb25Db3VudCI6MX0seyJpZCI6Ik5ZQzEiLCJkZXNpcmVkUmVwbGljYXRpb25Db3VudCI6MX1dLCJ2ZXJzaW9uIjoxfSwibWZhX2VuYWJsZWQiOmZhbHNlLCJzdGF0dXMiOiJBQ1RJVkUifSwiYXV0aGVudGljYXRpb25UeXBlIjoic2NvcGVkS2V5Iiwic2NvcGVkS2V5S2V5IjoiYThiZjY3YzcwNGYxNWYxM2I1N2MiLCJzY29wZWRLZXlTZWNyZXQiOiIxNWZjOWQzZDUzYzg4YTYwMDMzYTk4N2E0NjRlNjI0YmZlZjg1ZGY0NjNmMzAxZGYxY2Y0OGZkMTE5YzZjZjI0IiwiaWF0IjoxNzE0NzMxNTMyfQ.58HQmg0uKyShHYePFWCwkNSIaRm8le7rSO0z6mCEbBY";
app.use(express.json());
app.use(cors());

async function main( jsonData) {
    try {
        // Étape 1 : Création du fichier JSON
        const fileName = `${jsonData.id}.json`;
        fs.writeFileSync(fileName, JSON.stringify(jsonData));

        // Étape 2 : Création du formulaire avec le fichier à envoyer
        const formData = new FormData();
        const file = fs.createReadStream(fileName);
        formData.append("file", file);

        // Étape 3 : Ajout des métadonnées et options pour Pinata
        const pinataMetadata = JSON.stringify({
            name: fileName, // Vous pouvez personnaliser le nom du fichier ici
        });
        formData.append("pinataMetadata", pinataMetadata);

        const pinataOptions = JSON.stringify({
            cidVersion: 1,
        });
        formData.append("pinataOptions", pinataOptions);
        // Étape 4 : Envoi de la requête POST avec Axios
        const res = await axios.post(
            "https://api.pinata.cloud/pinning/pinFileToIPFS",
            formData,
            {
                headers: {
                    Authorization: `Bearer ${JWT}`,
                    ...formData.getHeaders() // Récupérer les en-têtes du formulaire pour Axios
                },
            }
        );

        // Étape 5 : Suppression du fichier après l'envoi
        fs.unlinkSync(fileName); // Supprimer le fichier JSON créé localement

        return res.data;
    } catch (error) {
        console.log(error);
    }
}


// Fonction pour obtenir l'URL du VC correspondant à un ID
function getVCURL(id) {
    const fileName = `${id}.json`;
    const filePath = `http://localhost:3002/VcsFolder/${fileName}`;
    return filePath;
}

// Exemple d'utilisation de la fonction côté serveur avec Express
app.post('/save-json', async (req, res) => {
    const jsonObject = req.body;

    try {
        const data = await main(jsonObject);
        console.log(data);
        res.send({result:"https://crimson-leading-pinniped-154.mypinata.cloud/ipfs/"+ data.IpfsHash});
    } catch (error) {
        console.error('Erreur:', error);
        res.status(500).send(error);
    }
});

// Exemple d'utilisation de la nouvelle méthode pour obtenir l'URL du VC correspondant à un ID
app.get('/vc-url/:id', (req, res) => {
    const id = req.params.id;
    const vcURL = getVCURL(id);
    res.status(200).send(vcURL);
});

app.listen(PORT, () => {
    console.log(`Serveur en cours d'exécution sur le port ${PORT}`);
});
